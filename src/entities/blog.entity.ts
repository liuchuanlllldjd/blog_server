import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  BeforeInsert,
  AfterLoad,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { User } from 'src/entities/user.entity';
import { Exclude } from 'class-transformer';

export enum BlogType {
  'unknow' = '',
  'FrontEnd' = '1', //前端
  'RearEnd' = '2', //后端
}

//文章表
@Entity()
export class Blog {
  @PrimaryGeneratedColumn()
  blogId: number; //文章id

  @ManyToOne((type) => User, (user) => user.userId)
  @JoinColumn({
    name: 'user',
  })
  user: number;

  @Column({
    comment: '标题',
  })
  title: string;

  @Column({
    type: 'text',
    comment: '文章内容',
  })
  content: string;

  @Column({
    type: 'enum',
    enum: BlogType,
    comment: '文章类型',
  })
  blogType: string;

  @Column({
    default: true,
    comment: '是否是草稿状态',
  })
  isDraft: boolean;

  @Column({
    default:
      'https://lio-1311014746.cos.ap-shanghai.myqcloud.com/defaultAcr.jpg',
    comment: '封面地址',
    length: 5000,
  })
  cover: string;

  // @OneToMany(() => Friend, (friend) => friend.user) // note: we will create author property in the Photo class below
  // friends: Friend[];

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @Column({
    default: false,
    comment: '是否删除',
  })
  isDelete: boolean;
}
