import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  BeforeInsert,
  AfterLoad,
} from 'typeorm';
import { Exclude } from 'class-transformer';

export enum Gender {
  'WOMEN' = '0',
  'MAN' = '1',
  'UNKNOWN' = '2',
}

//用户表
@Entity()
export class User {
  @PrimaryGeneratedColumn()
  userId: number; //用户id

  @Column({
    unique: true,
    comment: '用户账号',
  })
  username: string;

  @Column({
    default: 'DD用户',
    comment: '用户昵称',
  })
  nickname: string;

  @Exclude()
  @Column({
    comment: '用户密码',
  })
  password: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @Column({
    type: 'enum',
    enum: Gender,
    default: 2,
    comment: '用户性别',
  })
  gender: string;

  @Column({ nullable: true, comment: '用户邮箱' })
  email: string;

  @Column({ nullable: true, comment: '用户手机号' })
  phone: string;

  @Column({
    default:
      'https://lio-1311014746.cos.ap-shanghai.myqcloud.com/defaultAcr.jpg',
    comment: '头像地址',
    length: 5000,
  })
  avatar: string;

  @Column({
    default: false,
    comment: '是否删除',
  })
  isDelete: boolean;

  // @OneToMany(() => Friend, (friend) => friend.user) // note: we will create author property in the Photo class below
  // friends: Friend[];
}
