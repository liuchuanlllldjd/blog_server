import { User } from './../../entities/user.entity';
import {
  Controller,
  Get,
  Post,
  Body,
  Req,
  Param,
  Delete,
  Patch,
  Query,
  Request,
  UseGuards,
  DefaultValuePipe,
  ParseIntPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserService } from './user.service';
import {
  CreateUserDto,
  FindUserDto,
  UpdateUserDto,
  FindUserListDto,
} from 'src/dto/user.dto';
import { ApiTags, ApiOperation, ApiParam } from '@nestjs/swagger';
import { Public } from 'src/common/public.decorator';

@Controller('user')
@ApiTags('用户')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Public()
  @Post('register')
  @ApiOperation({ summary: '注册' })
  register(@Body() createUserDto) {
    return this.userService.create(createUserDto);
  }

  @ApiOperation({ summary: '根据token获取用户信息' })
  @Get('info')
  userInfo(@Request() request) {
    const { userId } = request.user;
    return this.userService.findOne({ userId });
  }

  @ApiOperation({ summary: 'cicd测试' })
  @Get('test')
  test(@Request() request) {
    return { message: 'cicd测试wawa3335555920129环境变量' };
  }

  @ApiOperation({ summary: '获取用户详情' })
  @Get(':userId')
  findOne(@Request() request, @Param('userId') userId: number) {
    return this.userService.findOne({ userId }, request.user);
  }

  @ApiOperation({ summary: '添加用户' })
  @Post('addUser')
  addUser(@Body() createUserDto) {
    const { nickname, username, password } = createUserDto;
    if (nickname) {
      if (!username) {
        createUserDto.username = nickname;
      }
      if (!password) {
        createUserDto.password = nickname;
      }
    }
    return this.userService.create(createUserDto);
  }

  @ApiOperation({ summary: '获取用户列表' })
  @Get()
  findAll(
    @Request() request,
    @Query('current', new DefaultValuePipe(1), ParseIntPipe) current: number,
    @Query('pageSize', new DefaultValuePipe(1), ParseIntPipe) pageSize: number,
    @Query()
    findUser: FindUserListDto,
  ) {
    findUser.current = current;
    findUser.pageSize = pageSize;
    return this.userService.findAll(findUser, request.user);
  }

  @Patch(':userId')
  @ApiOperation({ summary: '编辑用户' })
  updateUser(
    @Body('gender', new DefaultValuePipe(2), ParseIntPipe) gender: string,
    @Body('username') username: string,
    @Body() body,
    @Param('userId') userId: number,
  ) {
    return this.userService.updateUser({ gender, username, userId, ...body });
  }

  @Delete(':userId')
  @ApiOperation({ summary: '删除用户' })
  deleteUser(@Param('userId') userId: number) {
    return this.userService.deleteUser(userId);
  }

  @Delete()
  @ApiOperation({ summary: '批量删除用户' })
  deleteUserList(@Body('ids') ids: number[]) {
    return this.userService.deleteUserList(ids);
  }
}
