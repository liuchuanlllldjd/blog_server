import { Injectable } from '@nestjs/common';
import { HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository, FindOptionsWhere, Not, Between } from 'typeorm';
import {
  CreateUserDto,
  FindUserDto,
  FindUserListDto,
  UpdateUserDto,
} from 'src/dto/user.dto';
import { hashSync } from 'bcryptjs';
import { TypeOrmUtils } from 'src/utils/TypeOrmUtils';
import { User } from 'src/entities/user.entity';
// hashSync(密码，加密盐)加密
//compareSync(传入的数据,数据库的数据)判断是否相等

const sleep = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(1);
    }, 1000);
  });
};
@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async create(createUserParams: CreateUserDto) {
    await sleep();

    const { password } = createUserParams;
    const hasUser = await this.findOne({ username: createUserParams.username });

    if (hasUser.userId) {
      throw new HttpException({ message: '该用户已注册', showType: 1 }, 400);
    }
    createUserParams.password = hashSync(password, 10);
    const user = this.userRepository.create(createUserParams);
    this.userRepository.save(user);
    return { message: '新建用户成功' };
  }

  async findOne(findUser: FindUserDto, user?: User) {
    const hasUser = await this.userRepository.findOne({
      where: findUser,
    });
    if (user) {
      if (user.userId === hasUser.userId) return { ...hasUser };

      return { ...hasUser };
    } else return { ...hasUser };
  }

  async findAll(findUser: FindUserListDto, user) {
    const { gender, username, nickname, current, pageSize, start, end } =
      findUser;
    try {
      const where: {
        [key: string]: any;
      } = {
        isDelete: false,
        nickname: TypeOrmUtils.like(nickname),
        userId: Not(user.userId),
        username: TypeOrmUtils.like(username),
      };
      if (gender) {
        where.gender = gender;
      }
      if (start || end) {
        where.createdAt = Between(start, end);
      }
      const [list, total] = await this.userRepository.findAndCount({
        select: [
          'createdAt',
          'email',
          'gender',
          'userId',
          'avatar',
          'nickname',
          'phone',
          'username',
        ],

        where,
        order: {
          userId: 'ASC',
        },
        skip: (current - 1) * pageSize,
        take: pageSize,
      });
      return {
        list,
        total,
        current,
        pageSize,
      };
    } catch (error) {
      throw new HttpException(error, 500);
    }
  }

  async updateUser({ userId, ...others }: UpdateUserDto) {
    const user = await this.findOne({ userId });
    const newUser = Object.assign(user, others);
    await this.userRepository.save(newUser);
    return '修改成功';
  }

  /*
    软删除
  */
  async deleteUser(userId: number) {
    const user = await this.findOne({ userId });
    if (!user)
      throw new HttpException({ message: '无该用户', showType: 1 }, 400);
    user.isDelete = true;
    await this.userRepository.save(user);
    return '修改成功';
  }

  async deleteUserList(ids: number[]) {
    const res = this.userRepository.update(ids, { isDelete: true });
    return res;
  }
}
