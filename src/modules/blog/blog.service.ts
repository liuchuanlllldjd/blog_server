import {
  DefaultValuePipe,
  HttpException,
  HttpStatus,
  Injectable,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateBlogDto, FindAllBlogDto, UpdateBlogDto } from 'src/dto/blog.dto';
import { Blog } from 'src/entities/blog.entity';
import { Repository, Like, FindOperator } from 'typeorm';
import { BlogType } from 'src/entities/blog.entity';
import { TypeOrmUtils } from 'src/utils/TypeOrmUtils';

@Injectable()
export class BlogService {
  constructor(
    @InjectRepository(Blog)
    private readonly blogRepository: Repository<Blog>,
  ) {}
  async create(createBlogDto: CreateBlogDto, userId: number) {
    const blog = await this.blogRepository.create({
      ...createBlogDto,
      user: userId,
    });
    await this.blogRepository.save(blog);
    return blog;
  }
  async findAll(findAllParams: FindAllBlogDto) {
    const {
      current,
      pageSize,
      blogType,
      title,
      isDraft = false,
    } = findAllParams;
    try {
      const where: {
        isDelete: boolean;
        isDraft: boolean;
        blogType?: string;
        title?: FindOperator<string>;
      } = { isDraft, isDelete: false };
      if (blogType) {
        where.blogType = blogType;
      }
      if (title) {
        where.title = TypeOrmUtils.like(title);
      }
      const [list, total] = await this.blogRepository.findAndCount({
        select: [
          'blogId',
          'title',
          'content',
          'isDraft',
          'createdAt',
          'blogType',
          'updatedAt',
        ],
        where,
        relations: ['user'],
        order: {
          updatedAt: 'DESC',
        },
        skip: (current - 1) * pageSize,
        take: pageSize,
      });
      return {
        list,
        total,
        current,
        pageSize,
      };
    } catch (error) {
      throw new HttpException(
        { message: '查询错误', showType: 1 },
        HttpStatus.SERVICE_UNAVAILABLE,
      );
    }
  }

  async findOne(id: number) {
    try {
      const res = await this.blogRepository.findOne({
        select: [
          'blogId',
          'title',
          'content',
          'blogType',
          'createdAt',
          'updatedAt',
          'isDraft',
        ],
        relations: ['user'],
        where: {
          blogId: id,
        },
      });
      return res;
    } catch (error) {
      throw new HttpException(
        { message: '查询失败', showType: 2 },
        HttpStatus.SERVICE_UNAVAILABLE,
      );
    }
  }

  async update(id: number, updateBlogDto: UpdateBlogDto) {
    const blog = await this.findOne(id);
    const newBlog = Object.assign(blog, { ...updateBlogDto });
    const res = await this.blogRepository.save(newBlog);
    return res;
  }

  async remove(blogId: number) {
    const blog = await this.findOne(blogId);
    if (!blog)
      throw new HttpException({ message: '无该文章', showType: 1 }, 400);
    blog.isDelete = true;
    await this.blogRepository.save(blog);
    return '删除成功';
  }
}
