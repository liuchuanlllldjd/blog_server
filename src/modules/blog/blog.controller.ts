import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Request,
  Query,
  DefaultValuePipe,
  ParseIntPipe,
  ParseBoolPipe,
} from '@nestjs/common';
import { BlogService } from './blog.service';
import { CreateBlogDto, UpdateBlogDto } from 'src/dto/blog.dto';
import { BlogType } from 'src/entities/blog.entity';

@Controller('blog')
export class BlogController {
  constructor(private readonly blogService: BlogService) {}

  @Post()
  create(@Body() createBlogDto: CreateBlogDto, @Request() request) {
    const { userId } = request.user;
    return this.blogService.create(createBlogDto, userId);
  }

  @Get()
  findAll(
    @Query('current', new DefaultValuePipe(1), ParseIntPipe) current: number,
    @Query('pageSize', new DefaultValuePipe(1), ParseIntPipe) pageSize: number,
    @Query('blogType') blogType: BlogType,
    @Query('title') title: string,
  ) {
    const findAllParams = {
      current,
      pageSize,
      blogType,
      isDraft: false,
      title,
    };
    return this.blogService.findAll(findAllParams);
  }

  @Get('list')
  findUserBLog(
    @Query('current', new DefaultValuePipe(1), ParseIntPipe) current: number,
    @Query('isDraft', new DefaultValuePipe(false), ParseBoolPipe)
    isDraft: boolean,
  ) {
    const findAllParams = {
      current,
      pageSize: 10,
      isDraft,
    };
    return this.blogService.findAll(findAllParams);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.blogService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBlogDto: UpdateBlogDto) {
    return this.blogService.update(+id, updateBlogDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.blogService.remove(+id);
  }
}
