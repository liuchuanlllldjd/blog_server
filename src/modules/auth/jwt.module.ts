import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule as NestJwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    NestJwtModule.registerAsync({
      inject: [ConfigService],
      useFactory(configService: ConfigService) {
        return {
          secret: configService.get('jwtSecret'),
          signOptions: { expiresIn: '1d' },
        };
      },
    }),
  ],
  exports: [NestJwtModule],
})
export class JwtModule {}
