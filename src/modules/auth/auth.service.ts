import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { User } from 'src/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/modules/user/user.service';
import { compareSync } from 'bcryptjs';
@Injectable()
export class AuthService {
  private readonly userService: UserService;
  private readonly jwtService: JwtService;
  constructor(userService: UserService, jwtService: JwtService) {
    this.userService = userService;
    this.jwtService = jwtService;
  }

  async validate(username: string, password: string): Promise<User> {
    const hasUser = await this.userService.findOne({ username });
    if (!hasUser.password) {
      throw new HttpException(
        { message: '该用户未注册', showType: 2 },
        HttpStatus.BAD_REQUEST,
      );
    }
    // 注：实际中的密码处理应通过加密措施
    if (!compareSync(password, hasUser.password)) {
      throw new HttpException({ message: '该用户未注册', showType: 1 }, 400);
    }
    return hasUser;
  }

  async login(user: User): Promise<any> {
    const { username } = user;
    const { password, userId } = await this.userService.findOne({
      username,
    });
    return {
      token: this.jwtService.sign({ userId }),
    };
  }
}
