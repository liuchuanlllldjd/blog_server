import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Response,
} from '@nestjs/common';
import { CosService } from './cos.service';
import { getCredential } from 'qcloud-cos-sts';

import { config, policy } from './cos';
@Controller('cos')
export class CosController {
  constructor(private readonly cosService: CosService) {}
  @Post('sts')
  async sts() {
    const sts = await getCredential({
      secretId: config.secretId,
      secretKey: config.secretKey,
      proxy: config.proxy,
      durationSeconds: config.durationSeconds,
      endpoint: config.endpoint,
      policy: policy,
    });
    return {
      ...sts,
      Bucket: config.bucket,
      Region: config.region,
    };
  }
}
