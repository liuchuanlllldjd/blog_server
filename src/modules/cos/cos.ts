export const config = {
  secretId: 'AKIDkphNo0uXtw98tRlqgIf1EC4mHWVSL1Se', // 固定密钥
  secretKey: '3qQluoeRzGIFconX1FG2bw3vaCYeWDhz', // 固定密钥
  proxy: '',
  durationSeconds: 1800,
  // host: 'sts.tencentcloudapi.com', // 域名，非必须，默认为 sts.tencentcloudapi.com
  endpoint: 'sts.tencentcloudapi.com', // 域名，非必须，与host二选一，默认为 sts.tencentcloudapi.com

  // 放行判断相关参数
  bucket: 'blogfile-1311014746',
  region: 'ap-shanghai',
  allowPrefix: '*', // 这里改成允许的路径前缀，可以根据自己网站的用户登录态判断允许上传的具体路径，例子： a.jpg 或者 a/* 或者 * (使用通配符*存在重大安全风险, 请谨慎评估使用)
  // 简单上传和分片，需要以下的权限，其他权限列表请看 https://cloud.tencent.com/document/product/436/31923
  allowActions: [
    //简单上传操作
    'name/cos:PutObject',
    //表单上传对象
    'name/cos:PostObject',
    //分块上传：初始化分块操作
    'name/cos:InitiateMultipartUpload',
    //分块上传：List 进行中的分块上传
    'name/cos:ListMultipartUploads',
    //分块上传：List 已上传分块操作
    'name/cos:ListParts',
    //分块上传：上传分块操作
    'name/cos:UploadPart',
    //分块上传：完成所有分块上传操作
    'name/cos:CompleteMultipartUpload',
    //取消分块上传操作
    'name/cos:AbortMultipartUpload',
  ],
};

export const shortBucketName = config.bucket.substr(
  0,
  config.bucket.lastIndexOf('-'),
);
export const appId = config.bucket.substr(1 + config.bucket.lastIndexOf('-'));

export const policy = {
  version: '2.0',
  statement: [
    {
      action: config.allowActions,
      effect: 'allow',
      principal: { qcs: ['*'] },
      resource: [
        'qcs::cos:' +
          config.region +
          ':uid/' +
          appId +
          ':prefix//' +
          appId +
          '/' +
          shortBucketName +
          '/' +
          config.allowPrefix,
      ],
      // condition生效条件，关于 condition 的详细设置规则和COS支持的condition类型可以参考https://cloud.tencent.com/document/product/436/71306
      // 'condition': {
      //   // 比如限定ip访问
      //   'ip_equal': {
      //     'qcs:ip': '10.121.2.10/24'
      //   }
      // }
    },
  ],
};
