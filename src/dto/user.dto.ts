import { IsNotEmpty, IsString, IsByteLength, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Gender } from 'src/entities/user.entity';
export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  @IsByteLength(2, 10)
  @ApiProperty({ description: '账号', example: 'lio' })
  username: string;
  @IsNotEmpty()
  @IsString()
  @IsByteLength(6, 10)
  @ApiProperty({ description: '密码', example: 'lio' })
  password: string;

  @IsString()
  @ApiProperty({ description: '用户名', example: 'lio' })
  nickname?: string;

  @IsString()
  @ApiProperty({ description: '手机号', example: 'lio' })
  phone?: string;

  @IsNumber()
  @ApiProperty({ description: '性别', example: 'lio' })
  gender?: string;
}
export class UpdateUserDto {
  @IsNotEmpty()
  username?: string;
  gender?: string;
  userId: number;
}
export class FindUserDto {
  username?: string;
  nickname?: string;
  userId?: number;
  phone?: string;
  gender?: Gender;
}

export class FindUserListDto {
  @IsNotEmpty()
  current: number;
  pageSize: number;
  [propName: string]: any;
}
