import {
  IsNotEmpty,
  IsString,
  IsByteLength,
  IsNumber,
  IsBoolean,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { BlogType } from 'src/entities/blog.entity';

export class CreateBlogDto {
  @IsBoolean()
  @ApiProperty({ description: '是否是草稿', example: 'lio' })
  isDraft: boolean;

  @IsString()
  @ApiProperty({ description: '文章标题', example: 'lio' })
  title: string;

  @IsString()
  @ApiProperty({ description: '文章内容', example: 'lio' })
  content: string;

  @IsString()
  @ApiProperty({ description: '文章类型', example: '1' })
  blogType: string;
}

export class UpdateBlogDto {
  @IsString()
  @ApiProperty({ description: '文章标题', example: 'lio' })
  title: string;

  @IsString()
  @ApiProperty({ description: '文章内容', example: 'lio' })
  content: string;
}

export class FindAllBlogDto {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ description: '当前页', example: 1 })
  current: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ description: '每页条数', example: 10 })
  pageSize: number;

  @IsString()
  @ApiProperty({ description: '文章标题', example: '1' })
  title?: string;

  @IsString()
  @ApiProperty({ description: '文章类型', example: '1' })
  blogType?: string;

  @IsBoolean()
  @ApiProperty({ description: '是否是草稿', example: false })
  isDraft?: boolean;
}
