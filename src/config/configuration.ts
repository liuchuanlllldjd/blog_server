/**
 * 函数运行时区分env 变量不支持
 * @returns
 */

export type SqlConfigType = {
  username: string;
  password: string;
  host: string;
  database: string;
  port: number;
};

export type ConfigurationType = {
  // 端口
  port: number;
  projectName: string;

  // jwt secret
  jwtSecret: string;
  /**
   * 项目部署统一前缀
   */
  prefix: string;
  sqlConfig: SqlConfigType;
};

const findConfigModel = (): ConfigurationType => ({
  // 端口
  port: parseInt(process.env.PORT),
  projectName: process.env.PROJECT_NAME,

  // jwt secret
  jwtSecret: process.env.JWT_SECRET,
  /**
   * 项目部署统一前缀
   */
  prefix: process.env.PREFIX,
  sqlConfig: {
    username: process.env.SQL_USER_NAME,
    password: process.env.SQL_PASSWORD,
    host: process.env.SQL_HOST,
    database: process.env.SQL_DATA_BASE,
    port: Number(process.env.SQL_PORT),
  },
});

/**
 * 默认配置导出
 */
export default findConfigModel;
