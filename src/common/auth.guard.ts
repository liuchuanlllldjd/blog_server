//   /src/common/guards/auth.guard.ts
import {
  ExecutionContext,
  Injectable,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard } from '@nestjs/passport';
import { TokenExpiredError } from 'jsonwebtoken';
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private jwtService: JwtService, private reflector: Reflector) {
    super();
  }
  extractTokenFromHeader(req: any) {
    return req.headers.authorization?.split(' ')[1];
  }
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const isPublic = this.reflector.getAllAndOverride('isPublic', [
      context.getHandler(),
      context.getClass(),
    ]);

    if (isPublic) return true;
    const request = context.switchToHttp().getRequest();
    const token = this.extractTokenFromHeader(request);
    if (!token) {
      throw new HttpException(
        { message: '未登录', showType: 2 },
        HttpStatus.BAD_REQUEST,
      );
    }
    try {
      const payload = await this.jwtService.verifyAsync(token, {
        secret: process.env.JWT_SECRET,
      });

      // 💡 We're assigning the payload to the request object here
      // so that we can access it in our route handlers
      request['user'] = payload;
    } catch (error) {
      if (error instanceof TokenExpiredError) {
        throw new HttpException(
          { message: '登录过期', showType: 2 },
          HttpStatus.UNAUTHORIZED,
        );
      } else {
        throw new HttpException(
          { message: '无效登录信息', showType: 2 },
          HttpStatus.UNAUTHORIZED,
        );
      }
    }
    return true;
  }
}
