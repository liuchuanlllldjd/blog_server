import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';

import { Request, Response } from 'express';

interface ErrorObj extends HttpException {
  message: string;
  showType: number;
}

@Catch(HttpException)
export class HttpFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();
    const code = exception.getStatus();
    const res = exception.getResponse();
    let showType = 0;
    let errorMessage = '';
    if (typeof res === 'string') {
      errorMessage = res;
    } else {
      showType = (res as ErrorObj).showType;
      errorMessage = (res as ErrorObj).message;
    }

    response.status(code).json({
      success: false,
      data: {},
      errorCode: code,
      errorMessage,
      showType,
    });
  }
}
