FROM node:18.16.0

ARG RUNNING

# 设置时区
ENV TZ=Asia/Shanghai \
  DEBIAN_FRONTEND=noninteractive
ENV RUNNING $RUNNING

# 创建工作目录
RUN mkdir /app

# 指定工作目录
WORKDIR /app

#复制到工作目录
COPY . .

#install
RUN yarn

# 打包
RUN yarn build
# 运行
CMD if [ -z "$RUNNING" ] ; then echo '未传递环境变量'; else yarn up:$RUNNING; fi

# 复制当前所有代码到/app工作目录
#暴露端口3701（与服务启动端口一致）
EXPOSE 3701
